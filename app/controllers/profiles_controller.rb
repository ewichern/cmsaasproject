class ProfilesController < ApplicationController
   #use Devise gem's authenticate_user function to prevent unauthorized tampering
   before_action :authenticate_user!
   before_action :only_current_user

   def new
      # Display form for each user to add profile information
      @user = User.find(params[:user_id])
      @profile = Profile.new
   end

   def create
      @user = User.find(params[:user_id])
      @profile = @user.build_profile(profile_params)
      if @profile.save
         flash[:success] = "Profile updated!"
         redirect_to user_path(params[:user_id])
      else
         flash[:danger] = "Problem saving profile data."
         render action: :new
      end
   end

   def edit
      @user = User.find(params[:user_id])
      @profile = @user.profile
   end 

   def update
      @remove = params[:profile][:remove_avatar]
      @user = User.find(params[:user_id])
      @profile = @user.profile

      if @profile.update_attributes(profile_params)
         flash[:success] = "Profile updated!"
         redirect_to user_path(params[:user_id])
      else
         flash[:danger] = "Problem saving profile data."
         render action: :edit
      end

      if ( @remove == '1' )
         @profile.avatar.destroy
         @profile.save
         flash[:warning] = "Avatar deleted."
      end

   end

   private
   def profile_params
      params.require(:profile).permit(:first_name, :last_name, :job_title, :phone_number, :contact_email, :description, :avatar)
   end

   def only_current_user
      @user = User.find(params[:user_id])
      redirect_to(root_url) unless @user = current_user
   end

   def delete_avatar
      self.avatar = nil
      self.avatar.save
   end
end
