class Profile < ActiveRecord::Base
   belongs_to :user
   has_attached_file :avatar, :styles => { :large => "256x256>", :medium => "128x128>", :thumb => "64x64>" }, :default_url => "/images/:style/missing.png"
   validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
   validates_attachment_size :avatar, :less_than => 1.megabytes
   validates_attachment_file_name :avatar, :matches => [/png\Z/, /jpe?g\Z/, /gif\Z/]
   # validate :image_dimensions 
   do_not_validate_attachment_file_type :avatar
   
   attr_accessor :remove_avatar

   private
      def image_dimensions
         max_width = 300
         max_height = 300
         dimensions = Paperclip::Geometry.from_file(avatar.queued_for_write[:original].path)

         errors.add(:avatar, "Width must be less than #{max_width}px") unless dimensions.width <= max_width
         errors.add(:avatar, "Height must be less than #{ max_height }px") unless dimensions.height <= max_height
      end
end
